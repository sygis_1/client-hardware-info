package com.example.demo.bo;

import java.io.Serializable;

/**
 * @author 作者 zuoruibo: 
 * @date 创建时间：2020年11月12日 下午5:46:25 
 * @version 1.0
 * @parameter
 * @since
 * @return
 */
public class City implements Serializable{
	private static final long serialVersionUID = 1L;

	private Long id;

	private String name;

	private String state;

	private String country;

	private String path;
	private String type;
	private String engineType;
	private String ip;
	private String engineTypeCname;

	public String getEngineTypeCname() {
		return engineTypeCname;
	}

	public void setEngineTypeCname(String engineTypeCname) {
		this.engineTypeCname = engineTypeCname;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return getId() + "," + getName() + "," + getState() + "," + getCountry();
	}
}
