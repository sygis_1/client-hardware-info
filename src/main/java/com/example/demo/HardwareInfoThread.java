package com.example.demo;

import com.example.demo.util.CPUUtil;
import com.example.demo.util.DBUtil;
import com.example.demo.util.GPUUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HardwareInfoThread extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(HardwareInfoThread.class);

    private String clusterIp;

    public HardwareInfoThread(String clusterIp){
        this.clusterIp = clusterIp;
    }

    public void run() {
        while(true){
            try {
                logger.info("---------上报线程正在运行------");
                sleep(5*1000);
                InetAddress addr = null;
                try {
                    addr = InetAddress.getLocalHost();
                }catch (Exception e){
                    e.printStackTrace();
                }
                String ip = addr.getHostAddress();
                String hostname = addr.getHostName();

                // 判断客户端是否在集群里
                List<Map<Object,Object>> list = new ArrayList<>();
                try {
                    list = DBUtil.getEngineInfoByIPAndHostname(ip,hostname,clusterIp);
                }catch (SQLException e){
                    e.printStackTrace();
                }
                if(list.size()>0){
                    if(list.get(0).get("cluster_in").toString().equalsIgnoreCase("2")||list.get(0).get("cluster_ip")==null){
                        logger.info("不在集群内不用上报");
                    }else{
                        //---------------------------GPU------------------------------------------
                        List<Map<Object,Object>> res = new ArrayList<>();
                        try {
                            res = GPUUtil.getGPU();
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                        if(res.size()>0){
                            for(Map<Object,Object> map:res){
                                List<Map<Object,Object>> gpu_list = new ArrayList<>();
                                try {
                                    gpu_list = DBUtil.getGpuInfoByIP(map,ip,clusterIp);
                                }catch (SQLException e){
                                    e.printStackTrace();
                                }
                                if(gpu_list.size()>0){
                                    try {
                                        DBUtil.updateGpuInfo(map,ip,clusterIp);
                                    }catch (SQLException e){
                                        e.printStackTrace();
                                    }
                                }else{
                                    try {
                                        DBUtil.insertGpuInfo(map,ip,clusterIp);
                                    }catch (SQLException e){
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                        //---------------------------CPU------------------------------------------
                        Map<Object,Object> cpu = CPUUtil.printlnCpuInfo();
                        List<Map<Object,Object>> list_cpu = new ArrayList<>();
                        try {
                            list_cpu = DBUtil.getCpuInfoByIP(ip,clusterIp);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        if(list_cpu.size()>0){
                            try {
                                DBUtil.updateCpuInfo(cpu,ip,clusterIp);
                            }catch (SQLException e){
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                DBUtil.insertCpuInfo(cpu,ip,clusterIp);
                            }catch (SQLException e){
                                e.printStackTrace();
                            }
                        }
                        //---------------------------操作系统、内存--------------------------------------
                        Map<Object,Object> mem = CPUUtil.MemInfo();
                        List<Map<Object,Object>> list_mem = new ArrayList<>();
                        try {
                            list_mem = DBUtil.getMemInfoByIP(ip,clusterIp);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        if(list_mem.size()>0){
                            try {
                                DBUtil.updateMemInfo(mem,ip,hostname,clusterIp);
                            }catch (SQLException e){
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                DBUtil.insertMemInfo(mem,ip,hostname,clusterIp);
                            }catch (SQLException e){
                                e.printStackTrace();
                            }
                        }

                        //---------------------------引擎信息--------------------------------------
                        List<Map<Object,Object>> list_engine = new ArrayList<>();
                        try {
                            list_engine = DBUtil.getEngineInfoByIP(ip,clusterIp);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        if(list_engine.size()>0){
                            try {
                                DBUtil.updateEngineInfo(ip,hostname,"开机",2,clusterIp);
                            }catch (SQLException e){
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                DBUtil.insertEngineInfo(ip,hostname,"开机",2,clusterIp);
                            }catch (SQLException e){
                                e.printStackTrace();
                            }
                        }
                    }
                }else{
                    //---------------------------GPU------------------------------------------
                    List<Map<Object,Object>> res = new ArrayList<>();
                    try {
                        res = GPUUtil.getGPU();
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                    if(res.size()>0){
                        for(Map<Object,Object> map:res){
                            List<Map<Object,Object>> gpu_list = new ArrayList<>();
                            try {
                                gpu_list = DBUtil.getGpuInfoByIP(map,ip,clusterIp);
                            }catch (SQLException e){
                                e.printStackTrace();
                            }
                            if(gpu_list.size()>0){
                                try {
                                    DBUtil.updateGpuInfo(map,ip,clusterIp);
                                }catch (SQLException e){
                                    e.printStackTrace();
                                }
                            }else{
                                try {
                                    DBUtil.insertGpuInfo(map,ip,clusterIp);
                                }catch (SQLException e){
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    //---------------------------CPU------------------------------------------
                    Map<Object,Object> cpu = CPUUtil.printlnCpuInfo();
                    List<Map<Object,Object>> list_cpu = new ArrayList<>();
                    try {
                        list_cpu = DBUtil.getCpuInfoByIP(ip,clusterIp);
                    }catch (SQLException e){
                        e.printStackTrace();
                    }
                    if(list_cpu.size()>0){
                        try {
                            DBUtil.updateCpuInfo(cpu,ip,clusterIp);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                    }else{
                        try {
                            DBUtil.insertCpuInfo(cpu,ip,clusterIp);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                    }
                    //---------------------------操作系统、内存--------------------------------------
                    Map<Object,Object> mem = CPUUtil.MemInfo();
                    List<Map<Object,Object>> list_mem = new ArrayList<>();
                    try {
                        list_mem = DBUtil.getMemInfoByIP(ip,clusterIp);
                    }catch (SQLException e){
                        e.printStackTrace();
                    }
                    if(list_mem.size()>0){
                        try {
                            DBUtil.updateMemInfo(mem,ip,hostname,clusterIp);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                    }else{
                        try {
                            DBUtil.insertMemInfo(mem,ip,hostname,clusterIp);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                    }

                    //---------------------------引擎信息--------------------------------------
                    List<Map<Object,Object>> list_engine = new ArrayList<>();
                    try {
                        list_engine = DBUtil.getEngineInfoByIP(ip,clusterIp);
                    }catch (SQLException e){
                        e.printStackTrace();
                    }
                    if(list_engine.size()>0){
                        try {
                            DBUtil.updateEngineInfo(ip,hostname,"开机",2,clusterIp);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                    }else{
                        try {
                            DBUtil.insertEngineInfo(ip,hostname,"开机",2,clusterIp);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                    }
                }
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
