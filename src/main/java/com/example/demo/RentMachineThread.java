package com.example.demo;

import com.example.demo.util.CPUUtil;
import com.example.demo.util.DBUtil;
import com.example.demo.util.GPUUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RentMachineThread extends Thread {
    private static final Logger logger = LoggerFactory.getLogger(RentMachineThread.class);
    public void run() {
        while (true) {
            try {
                sleep(5*1000);
//                InetAddress addr = null;
//                try {
//                    addr = InetAddress.getLocalHost();
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//                String ip = addr.getHostAddress();
//                String hostname = addr.getHostName();
//                logger.info("----ip---"+ip);
//                logger.info("----hostname---"+hostname);
//                //---------------------------GPU------------------------------------------
//                List<Map<Object,Object>> res = new ArrayList<>();
//                try {
//                    res = GPUUtil.getGPU();
//                }catch (IOException e){
//                    e.printStackTrace();
//                }
//                if(res.size()>0){
//                    for(Map<Object,Object> map:res){
//                        List<Map<Object,Object>> gpu_list = new ArrayList<>();
//                        try {
//                            gpu_list = DBUtil.getGpuInfoByIP(map,ip);
//                        }catch (SQLException e){
//                            e.printStackTrace();
//                        }
//                        if(gpu_list.size()>0){
//                            try {
//                                DBUtil.updateGpuInfo(map,ip);
//                            }catch (SQLException e){
//                                e.printStackTrace();
//                            }
//                        }else{
//                            try {
//                                DBUtil.insertGpuInfo(map,ip);
//                            }catch (SQLException e){
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                }
//                //---------------------------CPU------------------------------------------
//                Map<Object,Object> cpu = CPUUtil.printlnCpuInfo();
//                List<Map<Object,Object>> list_cpu = new ArrayList<>();
//                try {
//                    list_cpu = DBUtil.getCpuInfoByIP(ip);
//                }catch (SQLException e){
//                    e.printStackTrace();
//                }
//                if(list_cpu.size()>0){
//                    try {
//                        DBUtil.updateCpuInfo(cpu,ip);
//                    }catch (SQLException e){
//                        e.printStackTrace();
//                    }
//                }else{
//                    try {
//                        DBUtil.insertCpuInfo(cpu,ip);
//                    }catch (SQLException e){
//                        e.printStackTrace();
//                    }
//                }
//                //---------------------------操作系统、内存--------------------------------------
//                Map<Object,Object> mem = CPUUtil.MemInfo();
//                List<Map<Object,Object>> list_mem = new ArrayList<>();
//                try {
//                    list_mem = DBUtil.getMemInfoByIP(ip);
//                }catch (SQLException e){
//                    e.printStackTrace();
//                }
//                if(list_mem.size()>0){
//                    try {
//                        DBUtil.updateMemInfo(mem,ip,hostname);
//                    }catch (SQLException e){
//                        e.printStackTrace();
//                    }
//                }else{
//                    try {
//                        DBUtil.insertMemInfo(mem,ip,hostname);
//                    }catch (SQLException e){
//                        e.printStackTrace();
//                    }
//                }
//
//                //---------------------------引擎信息--------------------------------------
//                List<Map<Object,Object>> list_engine = new ArrayList<>();
//                try {
//                    list_engine = DBUtil.getEngineInfoByIP(ip);
//                }catch (SQLException e){
//                    e.printStackTrace();
//                }
//                if(list_engine.size()>0){
//                    try {
//                        DBUtil.updateRentEngine(ip,hostname,"开机",1);
//                    }catch (SQLException e){
//                        e.printStackTrace();
//                    }
//                }else{
//                    try {
//                        DBUtil.insertRentEngine(ip,hostname,"开机",1);
//                    }catch (SQLException e){
//                        e.printStackTrace();
//                    }
//                }

                logger.info("---------上报结束------");
            }catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
