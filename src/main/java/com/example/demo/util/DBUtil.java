package com.example.demo.util;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBUtil {

    private final static String URL = "jdbc:mysql://192.168.188.156:3306/sy-yndk-cluster?characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8";
    private static final String USER = "root";
    private static final String PASSWORD = "sjyroot";
    private static Connection conn = null;


    public static Connection getConnection() {
        try {
            //1.加载驱动程序
            Class.forName("com.mysql.cj.jdbc.Driver");
            //2.获得数据库的连接
            conn = (Connection) DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static Connection getConnectionWithParam(String ip) {
        try {
            //1.加载驱动程序
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = "jdbc:mysql://"+ip+":3306/sy-yndk-cluster?characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8";
            //2.获得数据库的连接
            conn = (Connection) DriverManager.getConnection(url, USER, PASSWORD);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static void insertCpuInfo(Map<Object,Object> map,String ip,String clusterIp) throws SQLException {
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "insert into sys_cpu_info set ";
               sql += "server_ip='"+ip+"'";
               sql += ",cpu_cores='"+map.get("cpu_cores").toString()+"'";
               sql += ",cpu_sys_used_rate='"+map.get("cpu_sys_used_rate").toString()+"'";
               sql += ",cpu_user_used_rate='"+map.get("cpu_user_used_rate").toString()+"'";
               sql += ",cpu_wait_rate='"+map.get("cpu_wait_rate").toString()+"'";
               sql += ",cpu_sum_used_rate='"+map.get("cpu_sum_used_rate").toString()+"'";
               sql += ",cpu_degree='"+map.get("cpu_degree").toString()+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void insertNasEngine(String ip,String host_name,String on_off,Integer serverType) throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        String sql = "insert into sys_engine_info set ";
        sql += "server_ip='"+ip+"'";
        sql += ",host_name='"+host_name+"'";
        sql += ",on_off='"+on_off+"'";
        sql += ",in_time=now()";
        sql += ",server_type="+serverType;
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void insertRentEngine(String ip,String host_name,String on_off,Integer serverType) throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        String sql = "insert into sys_engine_info set ";
        sql += "server_ip='"+ip+"'";
        sql += ",host_name='"+host_name+"'";
        sql += ",on_off='"+on_off+"'";
        sql += ",in_time=now()";
        sql += ",server_type="+serverType;
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void insertEngineInfo(String ip,String host_name,String on_off,Integer serverType,String clusterIp) throws SQLException {
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String job_path = CCEngineUtill.getJobPath("CC_CENTER_JOBQUEUE");
        String havecommand = "cmd /c tasklist | findstr CCEngine.exe";
        String ishave = CCEngineUtill.runCommand(havecommand);
        String engineStatus = "";
        if (ishave == null) {
            engineStatus = "异常";
        } else {
            engineStatus = "正常";
        }
        String sql = "insert into sys_engine_info set ";
        sql += "server_ip='"+ip+"'";
        sql += ",host_name='"+host_name+"'";
        sql += ",on_off='"+on_off+"'";
        sql += ",engine_status='"+engineStatus+"'";
        sql += ",engine_type='AT,RasterProduction,TileProduction'";
        sql += ",type_name='全类型'";
        sql += ",in_time=now()";
        sql += ",job_path='"+job_path+"'";
        sql += ",server_type="+serverType;
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void saveEngineType(Map<Object,Object> map,String clusterIp) throws SQLException {
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "update sys_engine_info set ";
        sql += "engine_type='"+map.get("engine_type").toString()+"',";
        sql += "job_path='"+map.get("job_path").toString()+"',";
        sql += "type_name='"+map.get("type_name").toString()+"'";
        sql += " where server_ip='"+map.get("server_ip").toString()+"' and host_name='"+map.get("hostname").toString()+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void saveJobPath(Map<Object,Object> map,String clusterIp) throws SQLException {
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "update sys_engine_info set ";
        sql += "job_path='"+map.get("job_path").toString()+"'";
        sql += ",cc_path='"+map.get("cc_path").toString()+"'";
        sql += " where server_ip='"+map.get("server_ip").toString()+"' and host_name='"+map.get("hostname").toString()+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static Integer quitCluster(Map<Object,Object> client) throws SQLException {
        Connection conn = getConnectionWithParam(client.get("server_ip").toString());
        Statement stmt = conn.createStatement();
        Integer res = 0;
        String sql = "update sys_engine_info set cluster_ip=null,cluster_in=2,quit_time=now() where server_ip='"+client.get("server_ip").toString()+"' and host_name='"+client.get("hostname")+"'";
        try {
            res = stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        return res;
    }

    public static Integer joinCluster(Map<Object,Object> client) throws SQLException {
        Integer res = 0;
        List<Map<Object,Object>> list = getEngineInfoByHostname(client.get("hostname").toString(),client.get("cluster_ip").toString());
        if(list.size()>0){
            Connection conn = getConnectionWithParam(client.get("cluster_ip").toString());
            Statement stmt = conn.createStatement();
            String sql = "update sys_engine_info set ";
            String job_path = CCEngineUtill.getJobPath("CC_CENTER_JOBQUEUE");
            sql += "engine_status='异常'";
            sql += ",engine_type='AT,RasterProduction,TileProduction,AI'";
            sql += ",type_name='全类型'";
            sql += ",job_path='"+job_path+"'";
            sql += ",server_ip='"+client.get("server_ip").toString()+"'";
            sql += ",cluster_ip='"+client.get("cluster_ip").toString()+"'";
            sql += ",server_type=1";
            sql += ",cluster_in=1";
            sql += ",in_time=now()";
            sql += ",cc_path='"+client.get("cc_path").toString()+"'";
            sql += " where host_name='"+client.get("hostname").toString()+"'";
            try {
                res = stmt.executeUpdate(sql);
                stmt.close();
            }catch (Exception e){
                e.printStackTrace();
                stmt.close();
            }
            conn.close();
        }else{
            Connection conn = getConnectionWithParam(client.get("cluster_ip").toString());
            Statement stmt = conn.createStatement();
            String job_path = CCEngineUtill.getJobPath("CC_CENTER_JOBQUEUE");
            String sql = "insert into sys_engine_info set ";
            sql += "server_ip='"+client.get("server_ip").toString()+"'";
            sql += ",host_name='"+client.get("hostname").toString()+"'";
            sql += ",on_off='开机'";
            sql += ",engine_status='异常'";
            sql += ",job_path='"+job_path+"'";
            sql += ",cluster_ip='"+client.get("cluster_ip").toString()+"'";
            sql += ",engine_type='AT,RasterProduction,TileProduction,AI'";
            sql += ",type_name='全类型'";
            sql += ",in_time=now()";
            sql += ",server_type=1";
            sql += ",cc_path='"+client.get("cc_path").toString()+"'";
            try {
                res = stmt.executeUpdate(sql);
                stmt.close();
            }catch (Exception e){
                e.printStackTrace();
                stmt.close();
            }
            conn.close();
        }
        return res;
    }

    public static void operateEngineInfo(String ip,String engineType) throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        String havecommand = "cmd /c tasklist | findstr CCEngine.exe";
        String ishave = CCEngineUtill.runCommand(havecommand);
        String engineStatus = "";
        if (ishave == null) {
            engineStatus = "异常";
        } else {
            engineStatus = "正常";
        }
        String sql = "update sys_engine_info set ";
        sql += "engine_type='"+engineType+"'";
        sql += ",engine_status='"+engineStatus+"'";
        sql += " where server_ip='"+ip+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void updateEngineStopInfo(String ip,String host_name,String clusterIp) throws SQLException{
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "update sys_engine_info set ";
        sql += "engine_status='异常'";
        sql += " where server_ip='"+ip+"' and host_name='"+host_name+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void updateEngineRestartInfo(String ip,String host_name,String clusterIp) throws SQLException{
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "update sys_engine_info set ";
        sql += "engine_status='正常'";
        sql += " where server_ip='"+ip+"' and host_name='"+host_name+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void editEngineInfo(String ip,String engineType,String engineStatus) throws SQLException{
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        String sql = "update sys_engine_info set ";
        String job_path = CCEngineUtill.getJobPath("CC_CENTER_JOBQUEUE");
        sql += "engine_status='"+engineStatus+"'";
        sql += ",engine_type='"+engineType+"'";
        sql += ",job_path='"+job_path+"'";
        sql += " where server_ip='"+ip+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void updateNasEngine(String ip,String host_name,String on_off,Integer serverType) throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        String sql = "update sys_engine_info set ";
        sql += "host_name='"+host_name+"'";
        sql += ",on_off='"+on_off+"'";
        sql += ",server_type="+serverType;
        sql += " where server_ip='"+ip+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void updateRentEngine(String ip,String host_name,String on_off,Integer serverType) throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        String sql = "update sys_engine_info set ";
        sql += "host_name='"+host_name+"'";
        sql += ",on_off='"+on_off+"'";
        sql += ",server_type="+serverType;
        sql += " where server_ip='"+ip+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void updateEngineInfo(String ip,String host_name,String on_off,Integer serverType,String clusterIp) throws SQLException {
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
//        String job_path = CCEngineUtill.getJobPath("CC_CENTER_JOBQUEUE");
        String havecommand = "cmd /c tasklist | findstr CCEngine.exe";
        String ishave = CCEngineUtill.runCommand(havecommand);
        String engineStatus = "";
        if (ishave == null) {
            engineStatus = "异常";
        } else {
            engineStatus = "正常";
        }
        String sql = "update sys_engine_info set ";
        sql += "host_name='"+host_name+"'";
        sql += ",on_off='"+on_off+"'";
        sql += ",engine_status='"+engineStatus+"'";
//        sql += ",job_path='"+job_path+"'";
        sql += ",server_type="+serverType;
        sql += " where server_ip='"+ip+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void updateCpuInfo(Map<Object,Object> map,String ip,String clusterIp) throws SQLException {
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "update sys_cpu_info set ";
        sql += "cpu_cores='"+map.get("cpu_cores").toString()+"'";
        sql += ",cpu_sys_used_rate='"+map.get("cpu_sys_used_rate").toString()+"'";
        sql += ",cpu_user_used_rate='"+map.get("cpu_user_used_rate").toString()+"'";
        sql += ",cpu_wait_rate='"+map.get("cpu_wait_rate").toString()+"'";
        sql += ",cpu_sum_used_rate='"+map.get("cpu_sum_used_rate").toString()+"'";
        sql += ",cpu_degree='"+map.get("cpu_degree").toString()+"'";
        sql += " where server_ip='"+ip+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public  static  List<Map<Object,Object>> getEngineInfoByHostname(String hostname,String clusterIp) throws SQLException{
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "select server_ip,host_name,on_off,engine_status,engine_type,type_name,job_path,server_type,cluster_ip from sys_engine_info where host_name='"+hostname+"'";
        List<Map<Object,Object>> list = new ArrayList<>();
        try{
            ResultSet resultSet = stmt.executeQuery(sql);
            while (resultSet.next()) {//如果对象中有数据，就会循环打印出来
                Map<Object,Object> map = new HashMap<>();
                map.put("server_ip",resultSet.getString("server_ip"));
                map.put("host_name",resultSet.getString("host_name"));
                map.put("engine_status",resultSet.getString("engine_status"));
                map.put("engine_type",resultSet.getString("engine_type"));
                map.put("type_name",resultSet.getString("type_name"));
                map.put("job_path",resultSet.getString("job_path"));
                map.put("cluster_ip",resultSet.getString("cluster_ip"));
                list.add(map);
            }
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
        return list;
    }

    public  static  List<Map<Object,Object>> getEngineInfoByIPAndHostname(String ip,String hostname,String clusterIp) throws SQLException{
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "select server_ip,host_name,on_off,engine_status,engine_type,type_name,job_path,server_type,cluster_ip,cluster_in,cc_path from sys_engine_info where server_ip='"+ip+"' and host_name='"+hostname+"'";
        List<Map<Object,Object>> list = new ArrayList<>();
        try{
            ResultSet resultSet = stmt.executeQuery(sql);
            while (resultSet.next()) {//如果对象中有数据，就会循环打印出来
                Map<Object,Object> map = new HashMap<>();
                map.put("server_ip",resultSet.getString("server_ip"));
                map.put("host_name",resultSet.getString("host_name"));
                map.put("engine_status",resultSet.getString("engine_status"));
                map.put("engine_type",resultSet.getString("engine_type"));
                map.put("type_name",resultSet.getString("type_name"));
                map.put("job_path",resultSet.getString("job_path"));
                map.put("cluster_ip",resultSet.getString("cluster_ip"));
                map.put("cluster_in",resultSet.getString("cluster_in"));
                map.put("cc_path",resultSet.getString("cc_path"));
                list.add(map);
            }
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
        return list;
    }

    public static List<Map<Object,Object>> getEngineInfoByIP(String ip,String clusterIp) throws SQLException{
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "select server_ip,host_name,on_off,engine_status,engine_type,type_name,job_path,server_type,cluster_ip from sys_engine_info where server_ip='"+ip+"'";
        List<Map<Object,Object>> list = new ArrayList<>();
        try{
            ResultSet resultSet = stmt.executeQuery(sql);
            while (resultSet.next()) {//如果对象中有数据，就会循环打印出来
                Map<Object,Object> map = new HashMap<>();
                map.put("server_ip",resultSet.getString("server_ip"));
                list.add(map);
            }
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
        return list;
    }

    public static List<Map<Object,Object>> getCpuInfoByIP(String ip,String clusterIp) throws SQLException{
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "select server_ip from sys_cpu_info where server_ip='"+ip+"'";
        List<Map<Object,Object>> list = new ArrayList<>();
        try{
            ResultSet resultSet = stmt.executeQuery(sql);
            while (resultSet.next()) {//如果对象中有数据，就会循环打印出来
                Map<Object,Object> map = new HashMap<>();
                map.put("server_ip",resultSet.getString("server_ip"));
                list.add(map);
            }
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
        return list;
    }

    public static void insertMemInfo(Map<Object,Object> map,String ip,String hostname,String clusterIp) throws SQLException {
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "insert into sys_mem_info set ";
        sql += "server_ip='"+ip+"'";
        sql += ",host_name='"+hostname+"'";
        sql += ",sys_name='"+map.get("sys_name").toString()+"'";
        sql += ",sys_arch='"+map.get("sys_arch").toString()+"'";
        sql += ",mem_total='"+map.get("mem_total").toString()+"'";
        sql += ",mem_used='"+map.get("mem_used").toString()+"'";
        sql += ",mem_left='"+map.get("mem_left").toString()+"'";
        sql += ",mem_use_rate='"+map.get("mem_use_rate").toString()+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void updateMemInfo(Map<Object,Object> map,String ip,String hostname,String clusterIp) throws SQLException {
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "update sys_mem_info set ";
        sql += "host_name='"+hostname+"'";
        sql += ",sys_name='"+map.get("sys_name").toString()+"'";
        sql += ",sys_arch='"+map.get("sys_arch").toString()+"'";
        sql += ",mem_total='"+map.get("mem_total").toString()+"'";
        sql += ",mem_used='"+map.get("mem_used").toString()+"'";
        sql += ",mem_left='"+map.get("mem_left").toString()+"'";
        sql += ",mem_use_rate='"+map.get("mem_use_rate").toString()+"'";
        sql += " where server_ip='"+ip+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static List<Map<Object,Object>> getMemInfoByIP(String ip,String clusterIp) throws SQLException{
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "select server_ip from sys_mem_info where server_ip='"+ip+"'";
        List<Map<Object,Object>> list = new ArrayList<>();
        try{
            ResultSet resultSet = stmt.executeQuery(sql);
            while (resultSet.next()) {//如果对象中有数据，就会循环打印出来
                Map<Object,Object> map = new HashMap<>();
                map.put("server_ip",resultSet.getString("server_ip"));
                list.add(map);
            }
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
        return list;
    }


    public static List<Map<Object,Object>> getGpuInfoByIP(Map<Object,Object> obj,String ip,String clusterIp) throws SQLException{
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "select server_ip,gpu_no from sys_gpu_info where server_ip='"+ip+"' and gpu_no='"+obj.get("number").toString()+"'";
        List<Map<Object,Object>> list = new ArrayList<>();
        try{
            ResultSet resultSet = stmt.executeQuery(sql);
            while (resultSet.next()) {//如果对象中有数据，就会循环打印出来
                Map<Object,Object> map = new HashMap<>();
                map.put("server_ip",resultSet.getString("server_ip"));
                map.put("gpu_no",resultSet.getString("gpu_no"));
                list.add(map);
            }
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
        return list;
    }

    public static void insertGpuInfo(Map<Object,Object> map,String ip,String clusterIp) throws SQLException {
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String sql = "insert into sys_gpu_info set ";
        sql += "server_ip='"+ip+"'";
        sql += ",gpu_no='"+map.get("number").toString()+"'";
        sql += ",gpu_name='"+map.get("name").toString()+"'";
        sql += ",gpu_sum_mem='"+map.get("totalMemory").toString()+"'";
        sql += ",gpu_total_men="+Double.parseDouble(map.get("totalMemory").toString().replaceAll("G",""));
        sql += ",gpu_used_mem='"+map.get("usedMemory").toString()+"'";
        sql += ",gpu_used_rate='"+map.get("usageRate")+"'";
        sql += ",gpu_free_mem='"+map.get("useableMemory").toString()+"'";
        sql += ",gpu_degree='"+map.get("temperature")+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }

    public static void updateGpuInfo(Map<Object,Object> map,String ip,String clusterIp) throws SQLException {
        Connection conn = getConnectionWithParam(clusterIp);
        Statement stmt = conn.createStatement();
        String gpu_used_mem = map.get("usedMemory").toString();
        String gpu_used_rate = map.get("usageRate").toString();
        String gpu_free_mem = map.get("useableMemory").toString();
        String sql = "update sys_gpu_info set ";
        sql += "gpu_no='"+map.get("number").toString()+"'";
        sql += ",gpu_name='"+map.get("name").toString()+"'";
        sql += ",gpu_sum_mem='"+map.get("totalMemory").toString()+"'";
        sql += ",gpu_total_men="+map.get("totalMemory").toString().replaceAll("G","");
        sql += ",gpu_used_mem='"+gpu_used_mem+"'";
        sql += ",gpu_used_rate='"+gpu_used_rate+"'";
        sql += ",gpu_free_mem='"+gpu_free_mem+"'";
        sql += ",gpu_degree='"+map.get("temperature")+"'";
        sql += " where server_ip='"+ip+"'";
        try {
            stmt.executeUpdate(sql);
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
            stmt.close();
        }
        conn.close();
    }
}
