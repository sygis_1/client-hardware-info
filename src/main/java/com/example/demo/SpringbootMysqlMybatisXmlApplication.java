package com.example.demo;

import com.example.demo.util.DBUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.io.IOException;


@SpringBootApplication
public class SpringbootMysqlMybatisXmlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootMysqlMybatisXmlApplication.class, args);
//		HardwareInfoThread hardwareInfo = new HardwareInfoThread();
//		hardwareInfo.start();
//		if(args[0].equalsIgnoreCase("1")){ //租机
//			RentMachineThread rentMachineThread = new RentMachineThread();
//			rentMachineThread.start();
//        }else if(args[0].equalsIgnoreCase("2")){ // 跑模
//			HardwareInfoThread hardwareInfo = new HardwareInfoThread();
//			hardwareInfo.start();
//        }
	}
	
	/**
	 * 开启过滤器功能
	 * 
	 * @return
	 */
	private CorsConfiguration buildConfig() {
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.addAllowedOrigin("*");
		corsConfiguration.addAllowedHeader("*");
		corsConfiguration.addAllowedMethod("*");
		return corsConfiguration;
	}

	/**
	 * 跨域过滤器
	 * 
	 * @return
	 */
	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", buildConfig());
		return new CorsFilter(source);
	}

}
